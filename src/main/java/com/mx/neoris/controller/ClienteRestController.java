package com.mx.neoris.controller;

import com.mx.neoris.entity.Cliente;
import com.mx.neoris.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class ClienteRestController {

    @Autowired
    private IClienteService clienteService;

    @GetMapping("/clientes")
    public List<Cliente> getClientes() {
        return clienteService.findAll();
    }

    @GetMapping("/clientes/{id}")
    public ResponseEntity<?> clientePorId(@PathVariable Long id) {

        Cliente cliente = null;
        Map<String, Object> response = new HashMap<>();

        try {
        cliente = clienteService.findById(id);
        } catch(DataAccessException dae) {
            response.put("mensaje", "Error al realizar la consulta en la Base de Datos");
            response.put("error", dae.getMessage().concat(": ").concat(dae.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (cliente == null) {
            response.put("mensaje", "El cliente con ID: ".concat(id.toString().concat("No existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
    }

    @PostMapping("/clientes")
    public ResponseEntity<?> addCliente(@Validated @RequestBody Cliente cliente, BindingResult br) {

        Cliente clienteNew = null;
        Map<String, Object> response = new HashMap<>();

        if(br.hasErrors()) {
/*        List<String> errors = new ArrayList<>();
        for (FieldError error: br.getFieldErrors()) {
            errors.add("El campo '" + error.getField() +"' "+ error.getDefaultMessage());
        }*/

            List<String> errors = br.getFieldErrors()
                    .stream()
                    .map(e -> "El campo '" + e.getField() + "' " + e.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            clienteNew = clienteService.save(cliente);
        } catch(DataAccessException e) {
            response.put("mensaje", "Error al realizar el Insert en la Base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "El cliente ha sido generado con Exito!!");
        response.put("cliente", clienteNew);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @PutMapping("/clientes/{id}")
    public ResponseEntity<?> editCliente(@Validated @RequestBody Cliente cliente, BindingResult br, @PathVariable Long id) {

        Map<String, Object> response = new HashMap<>();
        Cliente c1 = clienteService.findById(id);
        Cliente clienteUpdated = null;

        if(br.hasErrors()) {
/*        List<String> errors = new ArrayList<>();
        for (FieldError error: br.getFieldErrors()) {
            errors.add("El campo '" + error.getField() +"' "+ error.getDefaultMessage());
        }*/

            List<String> errors = br.getFieldErrors()
                    .stream()
                    .map(e -> "El campo '" + e.getField() + "' " + e.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        if (c1 == null) {
            response.put("mensaje", "Error, no se puedo editar, el cliente ID: ".concat(id.toString().concat("No existe en la Base de Datos.")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        try {
            c1.setNombre(cliente.getNombre());
            c1.setApellido(cliente.getApellido());
            c1.setEmail(cliente.getEmail());
            c1.setCreateAt(cliente.getCreateAt());
            clienteUpdated = clienteService.save(c1);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar cliente en la Base de Datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "El cliente ha sido actualizado con Exito!!");
        response.put("cliente", clienteUpdated);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @DeleteMapping("/clientes/{id}")
    public ResponseEntity<?> deleteClientes(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();

        try {
        clienteService.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar cliente en la Base de Datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "El cliente ha sido eliminado con Exito");
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
    }

}
