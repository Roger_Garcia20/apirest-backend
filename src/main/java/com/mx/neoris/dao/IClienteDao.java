package com.mx.neoris.dao;

import com.mx.neoris.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface IClienteDao extends JpaRepository<Cliente, Long> {
}
